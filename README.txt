Miles Bright
Project 1

Development machine: Linux Mint 18.2 (Ubuntu 16.04)
GCC version 5.4.0
Python version 2.7
Testing machine: flip3


1. Compile chatclient.c with: gcc chatclient.c -o chatclient on host B.

2. Run chatserve.py with python chatserve.py <port number> on host A.

3. Run chatclient with ./chatclient <host A hostname> <host A port number> on host B.

4. Enter handle to chatclient program. The server will send an initial message.

5. Type a message into the chatclient and press enter.

6. Then switch to the server and type a message, then switch back to the client.

