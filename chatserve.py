# Miles Bright
# CS 372 - Project 1
# chatserve.py
# Usage: python chatserve.py portNumber

import sys
from socket import *

# Check arguments for usage errors
if len(sys.argv) > 2 or len(sys.argv) < 2:
  print("Usage: chatserve.py portNumber")
  sys.exit(0)

# Create socket on portNumber
serverPort = int(sys.argv[1])
serverSocket = socket(AF_INET, SOCK_STREAM)
serverSocket.bind(('', serverPort))
serverSocket.listen(1)

# Loop: Wait on the port for the client connection
serverRunning = 1
serverHandle = 'Harvey: '
messageBufferSize = 512
maxMessageSize = 500

while serverRunning:

  connectionSocket, addr = serverSocket.accept()
  initialMessage = serverHandle + 'Welcome to the server.'
  print(initialMessage)
  messageBuffer = ['\n'] * 512
  for i in range(0, len(initialMessage)):
    messageBuffer[i] = list(initialMessage)[i];
  connectionSocket.send(''.join(messageBuffer))

  # do server loop stuff
  clientAlive = 1
  while clientAlive:
    messageBuffer = connectionSocket.recv(messageBufferSize)
    # Check if message says '\quit'
    if messageBuffer.find("\\quit") != -1:
      connectionSocket.close()
      break
    # Strip newlines off messageBuffer
    messageArray = list(messageBuffer);
    for i in range(0, len(messageBuffer)):
      if messageArray[i] == '\n':
        messageArray[i] = '\0'
    print(''.join(messageArray))
    serverMessage = raw_input(serverHandle)
    while len(serverMessage) > 500:
      print('Message not sent. Must be 500 characters or less')
      serverMessage = raw_input(serverHandle)
    messageBuffer = ['\n'] * 512
    serverMessage = serverHandle + serverMessage
    for i in range(0, len(serverMessage)):
      messageBuffer[i] = list(serverMessage)[i]
    connectionSocket.send(''.join(messageBuffer))
    if serverMessage.find("\\quit") != -1:
      connectionSocket.close()
      break









