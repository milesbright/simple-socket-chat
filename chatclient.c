//----------------------------------------------------------------------------------
// Miles Bright
// CS 372 - Project 1
// chatclient.c
//
// Usage: chatclient server-hostname server-port
//----------------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/ioctl.h>


// Main function
// Input: host number via stdin
// Output: None
int main(int argc, char *argv[]) {

  // Check arguments and usage errors
  if (argc > 3 || argc < 3) {
    printf("Usage: %s server-hostname server-port\n", argv[0]);
    exit(0);
  }

  // Get the user's handle they want to use from stdin
  const int MAX_HANDLE_SIZE = 10;
  char *userHandle = (char *) malloc(sizeof(char) * (16));
  memset(userHandle, '-', 15);
  userHandle[15] = '\0';
  size_t bufferSize;
  while (strlen(userHandle) > MAX_HANDLE_SIZE) {
    printf("Enter a handle to chat with (max 10 characters): ");
    free(userHandle);
    userHandle = NULL;
    bufferSize = 0;
    getline(&userHandle, &bufferSize, stdin);
    userHandle[strcspn(userHandle, "\n")] = '\0';
  }

  // Initialize connection to the server
  struct sockaddr_in serverAddress;
  memset((char*) &serverAddress, '\0', sizeof(serverAddress));
  serverAddress.sin_family = AF_INET;
  serverAddress.sin_port = htons(atoi(argv[2]));
  struct hostent *serverHostInfo = gethostbyname(argv[1]);
  if (serverHostInfo == NULL) {
    fprintf(stderr, "Error: No such host %s\n", argv[1]);
    free(userHandle);
    exit(0);
  }
  memcpy((char*) &serverAddress.sin_addr.s_addr, (char*) serverHostInfo->h_addr, serverHostInfo->h_length);
  // Set up socket
  int socketFD = socket(AF_INET, SOCK_STREAM, 0);
  if (socketFD < 0) {
    fprintf(stderr, "Error opening socket\n");
    free(userHandle);
    exit(0);
  }
  // Connect to server
  if (connect(socketFD, (struct sockaddr*) &serverAddress, sizeof(serverAddress)) < 0) {
    fprintf(stderr, "Error connecting to server\n");
    free(userHandle);
    exit(0);
  }

  // Receive initial message from server
  const int MAX_MSG_SIZE = 500;
  int messageBufferSize = 512;
  char *messageBuffer = (char *) malloc(sizeof(char) * messageBufferSize);
  memset(messageBuffer, '\0', messageBufferSize);
  int charsRead, charsWritten;
  charsRead = recv(socketFD, messageBuffer, messageBufferSize, 0);
  // Replace all newlines with '\0'
  int i;
  for (i = 0; i < messageBufferSize; i++) {
    if (messageBuffer[i] == '\n') {
      messageBuffer[i] = '\0';
    }
  }
  printf("%s\n", messageBuffer);

  // Loop to send and receive messages back and forth
  int serverConnectionAlive = 1;
  char *userMessage = (char *) malloc(sizeof(char));
  while (serverConnectionAlive) {
    // Display prompt and get message to send from user
    printf("%s: ", userHandle);
    free(userMessage);
    userMessage = NULL;
    bufferSize = 0;
    getline(&userMessage, &bufferSize, stdin);
    if (strlen(userMessage) > 500) {
      printf("Message not sent. Must be 500 characters or less.\n");
      continue;
    }
    // Copy user's handle prompt and message to messageBuffer to send
    memset(messageBuffer, '\n', messageBufferSize);
    for (i = 0; i < strlen(userHandle); i++) {
      messageBuffer[i] = userHandle[i];
    }
    messageBuffer[strlen(userHandle)] = ':';
    messageBuffer[strlen(userHandle) + 1] = ' ';
    for (i = 0; i < strlen(userMessage); i++) {
      messageBuffer[i + strlen(userHandle) + 2] = userMessage[i];
    }

    // Send message to server and check for charsWritten. If -1 close(socketFD) and quit
    charsWritten = send(socketFD, messageBuffer, messageBufferSize, 0);
    if (charsWritten < 0) {
      close(socketFD);
      break;
    }
    // Check for the quit command
    if (strstr(userMessage, "\\quit") != NULL) {
      close(socketFD);
      break;
    }

    // Receive the server's message back
    memset(messageBuffer, '\0', messageBufferSize);
    charsRead = recv(socketFD, messageBuffer, messageBufferSize, 0);
    for (i = 0; i < messageBufferSize; i++) {
      if (messageBuffer[i] == '\n') {
        messageBuffer[i] = '\0';
      }
    }
    // Check for the quit command
    if (strstr(messageBuffer, "\\quit") != NULL) {
      close(socketFD);
      break;
    }
    printf("%s\n", messageBuffer);

  }

  free(userHandle);
  free(userMessage);
  free(messageBuffer);
  return 0;

}











































